
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>User Management System</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/css/app.css">
  <link rel="stylesheet" href="/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper" id="app">
  <!-- Navbar -->
  @include('includes.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="/img/home.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">UMS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <router-link tag="li" to="/dashboard" class="nav-link">
                <i class="fa fa-tachometer-alt"></i>
                <p>Dashboard</p>
              </router-link>
            </li>
            <li class="nav-item">
                <router-link to="/users" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        Users
                        <i class="fa fa-cog-alt right"></i>
                        <span class="badge badge-info right">6</span>
                    </p>
                </router-link>
            </li>
            @can('isAdmin')
            <li class="nav-item">
              <router-link to="/developer" class="nav-link">
                <i class="nav-icon fa fas fa-cog"></i>
                Developers
              </router-link>
            </li>
            @endcan
            <li class="nav-item">
              <router-link to="/profile" class="nav-link">
                <i class="nav-icon fa fas fa-user"></i>
                Profile
              </router-link>
            </li>
            <li class="nav-item">
                <router-link to="/roles" class="nav-link">
                    <i class="nav-icon fas fa-dot-circle"></i>
                    <p>
                        Roles
                        <i class="fa fa-cog-alt right"></i>
                        <span class="badge badge-info right">6</span>
                    </p>
                </router-link>
            </li>
            <li class="nav-item">
                <router-link to="/posts" class="nav-link">
                    <i class="nav-icon fas fa-file-powerpoint"></i>
                    <p>
                        Posts
                        <i class="fa file-powerpoint-alt right"></i>
                        <span class="badge badge-info right">6</span>
                    </p>
                </router-link>
            </li>
            <li class="nav-item" style="margin-left:-10px;">
              <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                <i class="fa fa-power-off fa-circle nav-icon"></i>
                                        {{ __('Log out') }}
              </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </li>
                
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" id="app">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="content-header">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h3 class="m-0 text-dark">Dashboard</h3>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
              </ol>
            </div><!-- /.col -->
         </div><!-- /.row -->
        </div>
      <div class="content-body">
        <router-view></router-view>
        <vue-progress-bar></vue-progress-bar>
      </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2018 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.0-beta.1
    </div>
  </footer>
</div>
<script src="/js/app.js"></script>
</body>
</html>
