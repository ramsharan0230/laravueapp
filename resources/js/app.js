require("./bootstrap");

window.Vue = require("vue");
import VueRouter from "vue-router";
import { Form, HasError, AlertError } from "vform";
import VueProgressBar from "vue-progressbar";
window.Form = Form;
import moment from "moment";

//form validator
import VeeValidate from "vee-validate";
Vue.use(VeeValidate);
import Datepicker from "vuejs-datepicker";
import Swal from "sweetalert2";
window.Swal = Swal;
const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 5000
});
window.Toast = Toast;
window.Fire = new Vue();

Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
Vue.use(VueProgressBar, options);
Vue.use(VueRouter);
const options = {
    color: "#bffaf3",
    failedColor: "#874b4b",
    thickness: "5px",
    transition: {
        speed: "0.2s",
        opacity: "0.6s",
        termination: 300
    },
    autoRevert: true,
    location: "left",
    inverse: false
};
Vue.component(
    "passport-clients",
    require("./components/passport/Clients.vue").default
);

Vue.component(
    "passport-authorized-clients",
    require("./components/passport/AuthorizedClients.vue").default
);

Vue.component(
    "passport-personal-access-tokens",
    require("./components/passport/PersonalAccessTokens.vue").default
);

var dashboard = require("./components/DashboardComponent.vue").default;
var profile = require("./components/ProfileComponent.vue").default;
var users = require("./components/UsersComponent.vue").default;
var developer = require("./components/DeveloperComponent.vue").default;
var roles = require("./components/RoleComponent.vue").default;
var posts = require("./components/PostComponent.vue").default;

let routes = [
    { path: "/dashboard", component: dashboard, name: dashboard },
    { path: "/profile", component: profile, name: profile },
    { path: "/users", component: users, name: users },
    { path: "/developer", component: developer, name: developer },
    { path: "/roles", component: roles, name: roles },
    { path: "/posts", component: posts, name: posts }
];

const router = new VueRouter({
    routes
});

const app = new Vue({
    router
}).$mount("#app");

Vue.filter("upText", function(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
});
Vue.filter("myDate", function(created) {
    return moment(created).format("MMMM Do YYYY");
});

export default {
    components: {
        Datepicker
    }
};
