<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_name'=>'request|min:3|max:15|unique:roles'
        ];
    }

    public function messages()
    {
        return [
            'role_name.required' => 'Role Name is required!',
            'role_name.alpha' => 'Role name mush have only Characters!'
        ];
    }
}
