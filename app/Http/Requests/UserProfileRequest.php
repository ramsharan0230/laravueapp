<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule; 

class UserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        
        return [
            'email' => 'required|unique:users,id,'.$this->route->parameters()['user']->id,
            'name' => 'required|string|max:50',
            'password' => 'sometimes|min:6|max:50',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Email is required!',
            'name.required' => 'Name is required!',
            'password.required' => 'Password is required!',
            'email.unique' => 'The email ID you entered already exist',
            'email.email' => 'Please enter a valid email address',
        ];
    }

}
