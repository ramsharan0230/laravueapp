<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserProfileRequest;
use App\User; 
use Image;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $validated = $request->validated();
        return User::Create([
            'name'=>$request['name'],
            'email'=>$request['email'],
            'type'=>$request['type'],
            'bio'=>$request['bio'],
            'photo'=>'user.png',
            'password'=>Hash::make($request['password'])
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function profile(){
        return auth('api')->user();
    }
    //profile Update
    public function updateUserInfo(UserProfileRequest $request){
        $file = $request->photo;
        $user = auth('api')->user();
        $currentPhoto = $user->photo;
        if ($file != $currentPhoto) {
            $imageName = User::imageHandling($file);
            \Image::make($file)->save(public_path('img/profile/').$imageName);
            $request->merge(['photo'=>$imageName]);

            $userPreviousPicture = public_path('img/profile').$currentPhoto;
            if($userPreviousPicture){
                @unlink($userPreviousPicture);
            }   
        }
        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request->password)]);
        }
        $user->update($request->all());
        return ['message'=>"Successfully Updated"];
    }


    public function update(UserRequest $request, $id)
    {
        $user = \App\User::findOrFail($id);
        //request validations
        $validated = $request->validated();

        $user->update($request->all());
        return ['message', "User updated Successfully"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id)->delete();
        return ["message", "User Has been deleted Successfully"];
    }
}
